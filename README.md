# README #

This is a repository to help with the demonstration of a development workflow.

## In this demo We will ##

* Explain a possible collaboration scheme in a developer team
* Show you how a developer might get the code from a central repository
* How they makes changes to the code
* How they might keep the work they are doing separate form the original code for the duration of solving their task
* How do these changes enter the local repository
* How these changes make it back to the remote / central repository
* How an organization might handle the review of new code entering the codebase
* And how some automated tools might help with these steps

## Tools used ##

Version control: Git
Source code repositrory hosting: Bitbucket
Git GUI: Sourcetree
Continous Integration: Bitbucket Pipelines

Deployment: Heroku
Runtime: Node
