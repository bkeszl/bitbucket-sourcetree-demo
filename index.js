import express from "express";
require('dotenv').config()


const app = express();
app.set('view engine', 'ejs')

app.get('/', (req, res) => {
  res.render('index')
})

app.use('/public', express.static('public'))

const port = process.env.PORT || 8080;
app.listen(port, () => {
    console.log("Server listening on port " + port);
});